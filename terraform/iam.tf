resource "aws_iam_role" "s3_to_sqs_lambda_role" {
  name = "s3_to_sqs_lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "s3_to_sqs_lambda_policy_s3" {
  name = "s3_to_sqs_lambda_policy_s3"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "s3:*",
        Resource = [
          "${aws_s3_bucket.s3_job_offer_bucket.arn}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "s3_to_sqs_lambda_policy_s3_attachment" {
  name = "s3_to_sqs_lambda_policy_s3_attachment"
  policy_arn = aws_iam_policy.s3_to_sqs_lambda_policy_s3.arn
  roles      = [aws_iam_role.s3_to_sqs_lambda_role.name]
}

resource "aws_iam_policy" "s3_to_sqs_lambda_policy_sqs" {
  name = "s3_to_sqs_lambda_policy_sqs"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "sqs:SendMessage",
        Resource = [
          aws_sqs_queue.job_offers_queue.arn
        ]
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "s3_to_sqs_lambda_policy_sqs_attachment" {
  name = "s3_to_sqs_lambda_policy_sqs_attachment"
  policy_arn = aws_iam_policy.s3_to_sqs_lambda_policy_sqs.arn
  roles      = [aws_iam_role.s3_to_sqs_lambda_role.name]
}

resource "aws_iam_policy" "s3_to_sqs_lambda_policy_cloudwatch" {
  name = "s3_to_sqs_lambda_policy_cloudwatch"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "s3_to_sqs_lambda_policy_cloudwatch_attachment" {
  name = "s3_to_sqs_lambda_policy_cloudwatch_attachment"
  policy_arn = aws_iam_policy.s3_to_sqs_lambda_policy_cloudwatch.arn
  roles      = [aws_iam_role.s3_to_sqs_lambda_role.name]
}

resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {
  name = "sqs_to_dynamo_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
      },
    ]
  })
}

resource "aws_iam_role_policy" "sqs_access" {
  name   = "sqs_access_policy"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        Resource = "${aws_sqs_queue.job_offers_queue.arn}"
      },
    ]
  })
}

resource "aws_iam_role_policy" "dynamodb_put" {
  name   = "dynamodb_put_policy"
  role   = aws_iam_role.sqs_to_dynamo_lambda_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "dynamodb:PutItem",
        Resource = "${aws_dynamodb_table.job-table.arn}"
      },
    ]
  })
}

# Créez un rôle IAM pour votre fonction Lambda job_api_lambda
resource "aws_iam_role" "job_api_lambda_role" {
  name = "job_api_lambda_role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

# Créez une politique IAM pour autoriser l'accès à DynamoDB
resource "aws_iam_policy" "job_api_lambda_policy" {
  name        = "job_api_lambda_policy"
  description = "Policy for Lambda to access DynamoDB"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action   = "dynamodb:*",
        Effect   = "Allow",
        Resource = aws_dynamodb_table.job-table.arn
      }
    ]
  })
}

# Attachez la politique IAM au rôle
resource "aws_iam_policy_attachment" "job_api_lambda_policy_attachment" {
  name       = "job_api_lambda_policy_attachment"
  policy_arn = aws_iam_policy.job_api_lambda_policy.arn
  roles      = [aws_iam_role.job_api_lambda_role.name]
}

