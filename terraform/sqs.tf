# resource aws_sqs_queue job_offers_queue_deadletter
resource "aws_sqs_queue" "job_offers_queue_deadletter" {
  name = "job-offers-dead-letter-queue"
}

# resource aws_sqs_queue job_offers_queue
resource "aws_sqs_queue" "job_offers_queue" {
  name = "job-offers-queue"
  
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.job_offers_queue_deadletter.arn,
    maxReceiveCount     = 4
  })
}


# resource aws_lambda_event_source_mapping sqs_lambda_trigger
resource "aws_lambda_event_source_mapping" "sqs_to_dynamo_lambda_mapping" {
  event_source_arn = aws_sqs_queue.job_offers_queue.arn
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  batch_size       = 10 # Vous pouvez ajuster cela en fonction de vos besoins
}

