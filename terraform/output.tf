# output base_url
output "api_gateway_url" {
  description = "API Gateway URL"
  value       = aws_apigatewayv2_stage.dev.invoke_url
}
